.data
	welcome:		.asciiz		"\nWelcome to this Guessing Game, human!\nPlease pick a number between 1 and 100.  Don't tell me what it is. \nMy first guess is  "
	str1:		.asciiz		".  How did I do? "
	str2_high:	.asciiz		"\n\nHumm, my guess was too high.  My next guess is "
	str3_low:		.asciiz		"\n\nOK, my guess was too low. My next guess is "
	end1:		.asciiz		"\n\nYay, I got it in "
	end2:		.asciiz		" tries!  Your number was "
	end3:		.asciiz		"\nWould you like me to guess again(y/n)?"
	answer:		.space		256
.text

	main:
		jal PROC_USER_PROMPT

		li $v0, 4
		la $a0, str1
		syscall

		jal L1
		
		move $t1, $v0
		jal END
		
		li $v0, 4
		la $a0, end3
		syscall
		
		la  $a0, answer
   		li  $a1, 3
   		li  $v0, 8
   		syscall

   		lb  $t4, 0($a0)

   		beq $t4, 'y', main
   		beq $t4, 'Y', main
		
		li $v0, 10
		syscall
		
		
	PROC_USER_PROMPT:
		subi $sp, $sp, 8
		sw $ra, 0($sp)	
		sw $a0, 4($sp)
		
		li $v0, 4
		la $a0, welcome
		syscall
		
		li $a1, 100
		li $v0, 42
		syscall
		
		addi $a0, $a0, 1
		move $t5, $a0
		move $v0, $a0
		li $v0, 1
		syscall
		
		lw $a0, 4($sp)
		lw $ra, 0($sp)
		addi $sp, $sp, 8
		
		jr $ra
		
		
	L1:
		subi $sp, $sp, 20
		sw $ra, 0($sp)
		sw $t1, 4($sp)
		sw $t2, 8($sp)
		sw $t3, 12($sp)
		sw $t4, 16($sp)
		
		li $t1, 0
		li $t2, 100
		li $t3, 0
		li $t4, 100
	Loop:
		
		li $v0, 5
		syscall
		
		move $a1, $v0
		li $v0, 1
		li $v1, -1
		li $t7, 0
		
		addi $t1, $t1, 1
		beq $a1, $v0, L2 #to high
		beq $a1, $v1, L3 #too low
		move $v0, $t1
		beq $a1, $t7, L
		
	L:
		lw $t4, 16($sp)
		lw $t3, 12($sp)
		lw $t2, 8($sp)
		lw $t1, 4($sp)
		lw $ra, 0($sp)
		addi $sp, $sp, 20
		
		jr $ra
		
		
	L2:
		li $v0, 4
		la $a0, str2_high
		syscall
		
		
		move $t2, $t5
		
		sub $t4, $t2, $t3
		subi $t4, $t4, 1
		
		move $a1, $t4
		li $v0, 42
		syscall
		addi $a0, $a0, 1
		addi $t4, $t4, 1
		
		add $a0, $a0, $t3 
		move $t5, $a0
		move $v0, $a0
		
		li $v0, 1
		syscall
		
		
		
		li $v0, 4
		la $a0, str1
		syscall

		j Loop
	L3:
		
		li $v0, 4
		la $a0, str3_low
		syscall
	
		move $t3, $t5
		
		bgt $t1, 1, L3_2
		
		li $t6, 0
		sub $t6, $t2, $t3
		move $a1, $t6
		li $v0, 42
		syscall
		addi $a0, $a0, 1
		add $a0, $a0, $t3
		
		move $t5, $a0
		move $v0, $a0
		
		li $v0, 1
		syscall
		
		
		
		li $v0, 4
		la $a0, str1
		syscall

		j Loop
	L3_2:
	
		
		sub $t4, $t2, $t3
		
		move $a1, $t4
		li $v0, 42
		syscall
		addi $a0, $a0, 1
		add $a0, $a0, $t3
		
		move $t5, $a0
		move $v0, $a0
		
		li $v0, 1
		syscall
		
		
		
		li $v0, 4
		la $a0, str1
		syscall

		j Loop
		
		
	END:
	
		
		subi $sp, $sp, 8
		sw $ra, 0($sp)	
		sw $a0, 4($sp)
		
		li $v0, 4
		la $a0, end1
		syscall
		
		move $a0, $t1
		li $v0, 1
		syscall
		
		li $v0, 4
		la $a0, end2
		syscall
		
		move $a0, $t5
		li $v0, 1
		syscall
		
		lw $a0, 4($sp)
		lw $ra, 0($sp)
		addi $sp, $sp, 8
		
		jr $ra
