.data
	welcome:		.asciiz		"\nWelcome to this Guessing Game, human!\nI'm thinking of a number between 1 and 100.\nSee if you can guess it!\nYour guess: "
	new_line:		.asciiz		"\n"
	s1:		.asciiz		"Your guess was too low!\nYour guess: "
	s2:		.asciiz		"Your guess was too high!\nYour guess: "
	end1: 		.asciiz		"You got it in "
	end2:  		.asciiz 	" tries!  My number was "
	end3:		.asciiz 	".\n\nWould you like to play again(y/n)?"
	answer:		.space		256
	
.text

				
	main:
		jal PROC_USER_PROMPT
		move $a2, $v0
		
		jal PROC_USER_PROMPT2
		move $a1, $a0

		jal L1
		
		move $v1, $t2
		jal END

		li $v0, 4
		la $a0, end3
		syscall
		
		la  $a0, answer
   		li  $a1, 3
   		li  $v0, 8
   		syscall

   		lb  $t4, 0($a0)

   		beq $t4, 'y', main
   		beq $t4, 'Y', main


   		 
		li $v0, 10
		syscall
		
		
		
	PROC_USER_PROMPT:
		subi $sp, $sp, 8
		sw $ra, 0($sp)	
		sw $a0, 4($sp)
		
		li $v0, 4
		la $a0, welcome
		syscall
		
		li $v0, 5
		syscall
		
		lw $a0, 4($sp)
		lw $ra, 0($sp)
		addi $sp, $sp, 8
		
		jr $ra
		
	PROC_USER_PROMPT2:
		subi $sp, $sp, 4
		sw $ra, 0($sp)


		li $a1, 100
		li $v0, 42
		syscall
		
		addi $a0, $a0, 1
		
		lw $ra, 0($sp)
		addi $sp, $sp, 4
		
		jr $ra
		
		
	L1:
		subi $sp, $sp, 4
		sw $ra, 0($sp)
		li $t2, 0
		
	Loop:
		addi $t2, $t2, 1
		blt $a2, $a1, L2
		bgt $a2, $a1, L3
		
		lw $ra, 0($sp)
		addi $sp, $sp, 4
		jr $ra
		
		
	L2:
		li $v0, 4
		la $a0, s1
		syscall
		
		li $v0, 5
		syscall
		

		
		move $a2, $v0
		j Loop
		
	L3:
		li $v0, 4
		la $a0, s2
		syscall
		
		li $v0, 5
		syscall
		

		
		move $a2, $v0
		j Loop
		
	END:
		subi $sp, $sp, 8
		sw $ra, 0($sp)	
		sw $a0, 4($sp)
		
		li $v0, 4
		la $a0, end1
		syscall
		
		move $a0, $v1
		li $v0, 1		
		syscall
		
		li $v0, 4
		la $a0, end2
		syscall
		
		move $a0, $a1
		li $v0, 1
		syscall
		
		lw $a0, 4($sp)
		lw $ra, 0($sp)
		addi $sp, $sp, 8
		
		jr $ra
