.data
	#store the string "Hello World!" into the variable named "hello"
	hello: .asciiz "Hello world!\n"

.text
	#load the "hello" label memory address into register $a0
	la $a0, hello
	#load the value 4 into $v0
	li $v0, 4
	#system call 4 prints a string
	syscall
	
	#load a immediately to the register $v0 so the system can call it and exit the program
	li $v0, 10
	#system call 10 exits the program
	syscall
