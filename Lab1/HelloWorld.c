//pulling in the "stdio.h" library
#include <stdio.h>

//create the main function which return an integer
int main() {
    //to print out the text "Hello World!"
    printf("Hello World!\n");
    //return 0 to indicate that the program is running without problem
    return 0;
}//main
