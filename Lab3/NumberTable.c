#include <stdio.h>
#include <math.h>


float main() {

    int number;

    printf("Enter an integer between 1 and 50\n");

    scanf("%d", &number);

    printf(" x  1/x^3  sqrt(x) log_3(x) 1.2^x\n");
    printf(" -  -----  ------- ------- -------\n");

    int fahr;

    for (fahr = 1; fahr <= number; fahr = fahr + 1)
        printf("%2d %3.5f %3.5f %3.5f %3.5f\n", fahr, 1/pow(fahr,3), sqrt(fahr), log(fahr)/log(3), pow(1.2, fahr));

} //main
