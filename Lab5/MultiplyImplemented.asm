.data
	prompt1: .asciiz "Please enter multiplier: "
	prompt2: .asciiz "Please enter multiplicand: "
	space: .asciiz "\n"

.text
	#prompt the user to multiplier
	li $v0, 4
	la $a0, prompt1
	syscall
	
	#get the user input
	li $v0, 5
	syscall
	
	#store the user input
	move $t1, $v0
	
	#print a line in between and prompt the user to enter the nultiplicand
	li $v0, 4
	la $a0, space
	syscall
	
	li $v0, 4
	la $a0, prompt2
	syscall
	
	#get the user input
	li $v0, 5
	syscall
	
	#store the user input
	move $t2, $v0


loop1:
	#$t1= multiplier $t2 = multiplicand
	andi $t3, $t1, 1 #check to see if multiplier = 0
	beq $t3, $zero, loop2 #if it is, jump to loop2
	addu $t0,$t0,$t2 #add multiplicand to product and place the result in product register
	
loop2:
	sll $t2,$t2,1     # shift multiplicand to the left one bit
	srl $t1,$t1,1     # shift multiplier to the right one bit
	bne $t1, $zero, loop1 #if the multiplier does not equal to 0 yet, jump back to loop1	

	#print a beautiful space and the final result
	li $v0, 4
	la $a0, space
	syscall
	
	li $v0, 1
	move $a0, $t0
	syscall
