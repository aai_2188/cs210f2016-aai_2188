.data
	prompt1: .asciiz "Please enter a number: "
	prompt2: .asciiz "Please enter another number: "
	space: .asciiz "\n"
	result: .asciiz "The result of the two numbers multiplied is: "

.text
	#prompt the user to enter a number
	li $v0, 4
	la $a0, prompt1
	syscall
	
	#get the user input
	li $v0, 5
	syscall
	
	#store the user input
	move $t0, $v0
	
	#print a line in between and prompt the user to enter another number
	li $v0, 4
	la $a0, space
	syscall
	
	li $v0, 4
	la $a0, prompt2
	syscall
	
	#get the user input
	li $v0, 5
	syscall
	
	#store the user input
	move $t1, $v0
	
	#multiply the two numbers and show the result
	li $v0, 4
	la $a0, space
	syscall
	
	li $v0, 4
	la $a0, result
	syscall
	
	mult $t0, $t1
	mflo $a0
	
	li $v0, 1
	syscall
	
	