.data
	hello:		.asciiz		"Hello world!\n"
	number1:	.word		42
	number2:	.half		21
	number3:	.half		1701
	number4:	.byte		73
	number5:	.half		-1701
	number6:	.byte		127
	number7:	.word		65536
	sum:		.word		0
	difference:	.word		0
	
	#######################
	#  ADDRESS   #  DATA  #
	#######################
	# 0x10010010 #  42  #
	# 0x10010011 #  42  #
	# 0x10010012 #  42  #
	# 0x10010013 #  42  #
	# 0x10010014 #  21  #
	# 0x10010015 #  21  #
	# 0x10010016 #1701#
	# 0x10010017 #1701#
	# 0x10010018 #  73  #
	# 0x10010019 #        #
	# 0x1001001a # -1701#
	# 0x1001001b #-1701#
	# 0x1001001c # 127 #
	# 0x1001001d #        #
	# 0x1001001e #        #
	# 0x1001001f #        #
	# 0x10010020 #65536#
	# 0x10010021 #65536#
	# 0x10010022 #65536#
	# 0x10010023 #65536#
	# 0x10010024 #        #
	# 0x10010025 #        #
	# 0x10010026 #        #
	# 0x10010027 #        #
	#######################


.text
	#load number 1 into $t1 and the rest of the numbers into the sequential registers
	lw $t1, number1
	lh $t2, number2
	lh $t3, number3
	lb $t4, number4
	lh $t5, number5
	lb $t6, number6
	lw $t7, number7	

	#add 1701 and -1701, store sum in register $t8
	add $t8, $t3, $t5
	#subtract -1701 from 1701 and store the difference in register $t9
	sub $t9, $t3, $t5
	
	#store the sum value from $t8 into sum
	sb $t8, sum
	#store the difference value from $t9 into difference
	sb $t9, difference
	
	la $a0, hello
	li $v0, 4
	syscall
	
	li $v0, 10
	syscall
