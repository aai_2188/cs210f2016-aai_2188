.data
	str:	.asciiz		"MIPS is awesome!"
	str1: 	.asciiz		"The string length is: "
	newline:	.asciiz		"\n"
	length: 	.word 0

.text

	la $t0, str			# $t0 holds the string address
	li $t1, 0			# $t1 holds the character count
	la $t3, str1			# $t3 holds the string address of str1
	
   loopTop:				# top of our loop
   	lb $t2, 0($t0)			# load the character at address $t0
   
   	bne $t2, $zero, notEqual	# jump to notEqual if things aren't equal
   	
   	# outputting the string str1
   	li $v0, 4
   	move $a0, $t3
   	syscall
   	
   	la     $a0, newline
   	addi   $v0, $0, 4
   	syscall
   	
   	# found our end of string
   	li $v0, 1			# setting syscall 1
   	move $a0, $t1			# copying the string length
   	syscall				# issuing the system call
   	
   	sw $t1, length
   	li $v0, 10			# setting syscall 10
   	syscall				# issuing the system call
   	
   	
  notEqual:
  	addi $t1, $t1, 1		# increment $t1
  	addi $t0, $t0, 1		# move to the next char
  	j loopTop			# jump to the top of the loop
