#include <stdio.h>
#include <math.h>

int main() {
    //asking the user to determine the first column
    int num1, num2, num3, max, i;
    printf("Which of the following columns should appear first? \n 1. 1/x^3\n 2. sqrt(x)\n 3. log_3(x)\n 4. 1.2^x\n");
    scanf("%i", &num1);

    //determine the second column based on the first column
    if(num1 == 1) {
        printf("Which of the following columns should appear second? \n 1. sqrt(x)\n 2. log_3(x)\n 3. 1.2^x\n");
        scanf("%i", &num2);

        if(num2 == 1) {
            printf("Which of the following columns should appear third? \n 1. log_3(x)\n 2. 1.2^x\n");
            scanf("%i", &num3);

            //determine the third and fourth column
            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1/x^3  \t sqrt(x)  \tlog_3(x)\t     1.2^x\n");
            	printf("\t -\t  -----  \t -------  \t--------\t     -----\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1/x^3  \t sqrt(x) \t  1.2^x  \t   log_3(x)\n");
            	printf("\t -\t  -----  \t ------- \t  -----  \t   --------\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;
            }
        } else if(num2 == 2){

            printf("Which of the following columns should appear third? \n 1. sqrt(x)\n 2. 1.2^x\n");
            scanf("%i", &num3);

            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1/x^3  \t log_3(x)  \tsqrt(x)\t     1.2^x\n");
            	printf("\t -\t  -----  \t --------  \t-------\t     -----\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1/x^3  \t log_3(x) \t  1.2^x  \t    sqrt(x)\n");
            	printf("\t -\t  -----  \t -------- \t  -----  \t    -------\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;
            }

        } else if(num2 == 3){
            printf("Which of the following columns should appear third? \n 1. sqrt(x)\n 2. log_3(x)\n");
            scanf("%i", &num3);

            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1/x^3  \t 1.2^x   \t sqrt(x)\t   log_3(x)\n");
            	printf("\t -\t  -----  \t -----   \t -------\t   --------\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1/x^3  \t  1.2^x \t  log_3(x)  \t  sqrt(x)\n");
            	printf("\t -\t  -----  \t  ----- \t  --------  \t  -------\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;
            }

        }

    } else if(num1 == 2) {
        printf("Which of the following columns should appear second? \n 1. 1/x^3\n 2. log_3(x)\n 3. 1.2^x\n");
        scanf("%i", &num2);

        if(num2 == 1) {
            printf("Which of the following columns should appear third? \n 1. log_3(x)\n 2. 1.2^x\n");
            scanf("%i", &num3);

            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  sqrt(x)  \t  1/x^3  \tlog_3(x)\t     1.2^x\n");
            	printf("\t -\t  -------  \t  -----  \t--------\t     -----\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  sqrt(x)  \t  1/x^3  \t  1.2^x  \t   log_3(x)\n");
            	printf("\t -\t  -------  \t  -----  \t  -----  \t   --------\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;
            }

        } else if(num2 == 2){

            printf("Which of the following columns should appear third? \n 1. 1/x^3 \n 2. 1.2^x\n");
            scanf("%i", &num3);

            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  sqrt(x)  \t log_3(x)  \t 1/x^3\t     1.2^x\n");
            	printf("\t -\t  -------  \t --------  \t -----\t     -----\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

           	    printf("\t x\t  sqrt(x)  \t log_3(x)  \t 1.2^x   \t    1/x^3\n");
            	printf("\t -\t  -------  \t --------  \t ------  \t    -------\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;
            }

        } else if(num2 == 3){
            printf("Which of the following columns should appear third? \n 1. 1/x^3\n 2. log_3(x)\n");
            scanf("%i", &num3);

            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  sqrt(x)  \t  1.2^x \t  1/x^3 \t   log_3(x)\n");
            	printf("\t -\t  -------  \t  ----- \t  ----- \t   --------\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  sqrt(x)  \t  1.2^x \t  log_3(x)  \t  1/x^3\n");
            	printf("\t -\t  -------  \t  ----- \t  --------  \t  -----\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;
            }
        }
    } else if(num1 == 3) {
        printf("Which of the following columns should appear second? \n 1. 1/x^3\n 2. sqrt(x)\n 3. 1.2^x\n");
        scanf("%i", &num2);

        if(num2 == 1) {
            printf("Which of the following columns should appear third? \n 1. sqrt(x)\n 2. 1.2^x\n");
            scanf("%i", &num3);

            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  log_3(x)  \t  1/x^3  \t sqrt(x)\t     1.2^x\n");
            	printf("\t -\t  --------  \t  -----  \t -------\t     -----\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  log_3(x)  \t  1/x^3  \t  1.2^x  \t  sqrt(x)\n");
            	printf("\t -\t  --------  \t  -----  \t  -----  \t  -------\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;
            }

    } else if(num2 == 2){

            printf("Which of the following columns should appear third? \n 1. 1/x^3 \n 2. 1.2^x\n");
            scanf("%i", &num3);

            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  log_3(x)  \t  sqrt(x)  \t 1/x^3\t     1.2^x\n");
            	printf("\t -\t  --------  \t  -------  \t -----\t     -----\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

           	    printf("\t x\t  log_3(x)  \t sqrt(x)  \t 1.2^x   \t    1/x^3\n");
            	printf("\t -\t  --------  \t -------  \t ------  \t    -----\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;
            }

    } else if(num2 == 3){

            printf("Which of the following columns should appear third? \n 1. 1/x^3\n 2. sqrt(x)\n");
            scanf("%i", &num3);

            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  log_3(x)  \t  1.2^x \t  1/x^3 \t   sqrt(x)\n");
            	printf("\t -\t  --------  \t  ----- \t  ----- \t   -------\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  log_3(x)  \t  1.2^x \t  sqrt(x)  \t  1/x^3\n");
            	printf("\t -\t  --------  \t  ----- \t  -------  \t  -----\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;
            }
        }
    } else if(num1 == 4) {
        printf("Which of the following columns should appear second? \n 1. 1/x^3\n 2. sqrt(x)\n 3. log_3(x)\n");
        scanf("%i", &num2);

        if(num2 == 1) {
            printf("Which of the following columns should appear third? \n 1. sqrt(x)\n 2. log_3(x)\n");
            scanf("%i", &num3);

            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1.2^x  \t  1/x^3   \t sqrt(x)\t   log_3(x)\n");
            	printf("\t -\t  -----  \t  -----   \t -------\t   --------\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1.2^x  \t  1/x^3 \t  log_3(x)  \t  sqrt(x)\n");
            	printf("\t -\t  -----  \t  ----- \t  --------  \t  -------\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;
            }
        } else if (num2 == 2) {
            printf("Which of the following columns should appear third? \n 1. 1/x^3\n 2. log_3(x)\n");
            scanf("%i", &num3);

            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1.2^x  \t  sqrt(x) \t  1/x^3 \t   log_3(x)\n");
            	printf("\t -\t  -----  \t  ------- \t  ----- \t   --------\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1.2^x  \t  sqrt(x) \t  log_3(x)  \t  1/x^3\n");
            	printf("\t -\t  -----  \t  ------- \t  --------  \t  -----\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;
            }
        } else if (num2 == 3) {
            printf("Which of the following columns should appear third? \n 1. 1/x^3\n 2. sqrt(x)\n");
            scanf("%i", &num3);

            if(num3 == 1) {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1.2^x  \t  log_3(x) \t  1/x^3 \t   sqrt(x)\n");
            	printf("\t -\t  -----  \t  -------- \t  ----- \t   -------\n");
            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for

            	return 0;

            } else {
            	printf("Enter an integer between 1 and 50:");
            	scanf("%d",&max);

            	printf("\t x\t  1.2^x  \t  log_3(x) \t  sqrt(x)  \t  1/x^3\n");
            	printf("\t -\t  -----  \t  -------- \t  -------  \t  -----\n");

            	for (i = 1; i <= max; i++) {
                    printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
            	} //for
            	return 0;
            }
        }
    }
    return 0;
} //main
