.data
	str:		.asciiz		"MIPS_is_awesome!"
	newline:	.asciiz		"\n"

.text

	add $t2, $t0, $t1

	la $t0, str			
	li $t1, 0			
	move $t8, $t0			
	li $t9, 0		
	li $t7, 0x20		
	
	move $t3, $t0
	
	
   LOOPTop:		
   	
   	lb $t2, 0($t0)		
   
   	#go to notEqual if $t2 is not equal to 0
   	bne $t2, $zero, notEqual
   	
 	#decrement $t0
   	subi $t0, $t0, 1
   	
   	# $t0 is now the last character in the string, so let's put that into $t9
   	move $t9, $t0
   	
   	# And also start $t0 back at the beginning of the string
   	move $t0, $t8
   	
        # And print the string
        li $v0, 4
        la $a0, str
        syscall
        
        # And print a newline
        li $v0, 4
        la $a0, newline
        syscall
   	
   printLOOP:
        # loop until $t8 and $t9 is equal
        beq $t8, $t9, exit
   	
	# Remove a character from the beginning and update the pointer
   	sb $t7, 0($t8)
   	addi $t8, $t8, 1
   	
   	#print the string
   	li $v0, 4
   	la $a0, str
   	syscall
   	
   	#print a new line
        li $v0, 4
        la $a0, newline
        syscall
   	
	#check to see if $t8 and $t9 are equal
	beq $t8, $t9, exit
	
	#remove the ending character of the string
	sb $t7, 0($t9)
	subi $t9, $t9, 1
	
	# And print the string
	li $v0, 4
	la $a0, str
	syscall
	
        # print a new ine
        li $v0, 4
        la $a0, newline
        syscall	
	
	# jump to printlOOP
	j printLOOP
  	
   	
  notEqual:
  	addi $t1, $t1, 1		# increment $t1
  	addi $t0, $t0, 1		# move to the next char
  	j LOOPTop			# jump back to the top of the loop

  exit:
	# exit
    	li $v0, 10
   	syscall
